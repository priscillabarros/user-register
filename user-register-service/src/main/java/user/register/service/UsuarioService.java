package user.register.service;

import java.util.Set;

import user.register.dto.base.MessageApiDTO;
import user.register.dto.usuario.UsuarioDTO;
import user.register.service.exception.BusinessException;

/**
 * @author priscilla.barros
 * Classe de interface de serviços
 */

public interface UsuarioService {

	/**
	 * Consultar Usuario cadastrados em memory database (H2) - All
	 * 
	 * @return {@link Set<UsuarioDTO>} - Usuario em memory database
	 * @throws BusinessException 
	 */
	public Set<UsuarioDTO> retrieveUsuario();

	/**
	 * Consultar Usuario por ID
	 * 
	 * @return {@link UsuarioDTO} - Usuario em memory database por ID
	 * @throws BusinessException 
	 */
	public UsuarioDTO retrieveUsuarioById(Long id);
	
	/**
	 * Criar Usuario a partir de dados inseridos
	 * 
	 * @return {@link MessageApiDTO} - Retorno com mensagem estática e ID gerado para item criado
	 * @throws BusinessException 
	 */
	public MessageApiDTO createUsuario(UsuarioDTO dto);

	public MessageApiDTO removeUsuario(Long id);

	public MessageApiDTO updateUsuarioById(Long id, UsuarioDTO dtoIn);

}
