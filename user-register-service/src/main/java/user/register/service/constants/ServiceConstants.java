package user.register.service.constants;

/**
 * @author priscilla.barros
 * Constantes para camada de serviços
 */
public final class ServiceConstants {

	private ServiceConstants() {

	}	
	public static final String ENTITY_POST = "Usuario gravado com sucesso.";
	public static final String ENTITY_PUT = "Usuario atualizado com sucesso.";
	public static final String ENTITY_DELETE = "Usuario deletado com sucesso.";
	public static final String SEPARATOR = ",";
	
}
