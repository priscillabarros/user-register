package user.register.service.impl;

import user.register.service.constants.ServiceConstants;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import user.register.dto.base.MessageApiDTO;
import user.register.dto.usuario.UsuarioDTO;
import user.register.persistence.UsuarioDAO;
import user.register.service.UsuarioService;
import user.register.service.exception.BusinessException;
import user.register.service.util.ResponseUtil;

/**
 * @author priscilla.barros
 * Classe de implementação das regras de negócio (camada de serviços)
 */
@Service
public class UsuarioServiceImpl implements UsuarioService {	

	@Autowired
	private UsuarioDAO usuarioDAO;
	
	@Override
	public UsuarioDTO retrieveUsuarioById(Long id) {
		return usuarioDAO.find(id)
			   .orElseThrow(() -> new BusinessException("Usuario não encontrado com ID informado: " + id));
	
	}

	@Override
	public Set<UsuarioDTO> retrieveUsuario() {
		Set<UsuarioDTO> dto = usuarioDAO.findAll();
	
		if(dto.isEmpty()) {
			throw new BusinessException("Não foram encontrados registros para Usuario na base de dados.");
		}
		return dto;
	}

	@Override
	public MessageApiDTO createUsuario(UsuarioDTO dto) {		
		dto.setId(usuarioDAO.getSequence());
		
		return ResponseUtil.mountResponse(usuarioDAO.save(dto), ServiceConstants.ENTITY_POST);
	}
	
	@Override
	public MessageApiDTO removeUsuario(Long id) {
		usuarioDAO.removeUsuario(id);
		return ResponseUtil.mountResponse(id, ServiceConstants.ENTITY_DELETE);
	}
	
	@Override
	public MessageApiDTO updateUsuarioById(Long id, UsuarioDTO dtoIn) {
		
		UsuarioDTO dtoBd = retrieveUsuarioById(id);
		dtoBd.setNome(dtoIn.getNome());
		dtoBd.setNumeroDocumento(dtoIn.getNumeroDocumento());
		dtoBd.setTelefoneContato(dtoIn.getTelefoneContato());
		
		usuarioDAO.updateUsuario(dtoBd);
		
		return ResponseUtil.mountResponse(id, ServiceConstants.ENTITY_PUT);
	}
}
