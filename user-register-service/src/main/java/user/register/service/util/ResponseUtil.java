package user.register.service.util;

import org.springframework.http.HttpStatus;

import user.register.dto.base.MessageApiDTO;

/**
 * @author priscilla.barros
 * Classe de utils para montar resposta das operações de API
 */
public final class ResponseUtil {

	public static MessageApiDTO mountResponse(Long id, String mensagem) {
		return new MessageApiDTO(id, HttpStatus.OK.value(), mensagem);
	}
	
}
