package user.register;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author priscilla.barros
 * Classe main para execução da aplicação
 */

@SpringBootApplication
@ComponentScan(basePackages = "user.register")
public class UserRegisterApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserRegisterApplication.class, args);
	}
}
