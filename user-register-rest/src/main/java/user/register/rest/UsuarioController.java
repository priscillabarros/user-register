package user.register.rest;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import user.register.dto.base.MessageApiDTO;
import user.register.dto.usuario.UsuarioDTO;
import user.register.service.UsuarioService;

/**
 * @author priscilla.barros
 * Controller JDBI
 */
@RestController
@Api(value = "Usuario Controller") 
@RequestMapping("usuario")
@CrossOrigin
public class UsuarioController {
	
	@Autowired
	private UsuarioService service;	
	
	@GetMapping
	@ApiOperation(value = "Operação para consultar todas as Usuarios sem filtro", produces = APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = UsuarioDTO.class, message = "Sucesso"),
			@ApiResponse(code = 400, message = "Bad Request"),
			@ApiResponse(code = 500, message = "Internal Server Error")
	})
	public Set<UsuarioDTO> retrieveUsuario() {
		return service.retrieveUsuario();
	}
	
	@GetMapping("{id}")
	@ApiOperation(value = "Operação para consultar Usuario por ID", produces = APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = UsuarioDTO.class, message = "Sucesso"),
			@ApiResponse(code = 400, message = "Bad Request"),
			@ApiResponse(code = 500, message = "Internal Server Error")
	})
	public UsuarioDTO retrieveUsuarioById(@PathVariable Long id) {
		return service.retrieveUsuarioById(id);
	}
	
	@PostMapping
	@ApiOperation(value = "Operação para cadastrar Usuario", produces = APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = MessageApiDTO.class, message = "Sucesso"),
			@ApiResponse(code = 400, message = "Bad Request"),
			@ApiResponse(code = 500, message = "Internal Server Error")
	})
	public MessageApiDTO createUsuario(@RequestBody @Valid UsuarioDTO dto) {
		return service.createUsuario(dto);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Operação para remover Usuario", produces = APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = MessageApiDTO.class, message = "Sucesso"),
			@ApiResponse(code = 400, message = "Bad Request"),
			@ApiResponse(code = 500, message = "Internal Server Error")
	})
	public MessageApiDTO removeUsuario(@PathVariable Long id) {
		return service.removeUsuario(id);
	}
	
	@PutMapping("{id}")
	@ApiOperation(value = "Operação para atualizar Usuario", produces = APPLICATION_JSON_VALUE)
	@ApiResponses(value = {
			@ApiResponse(code = 200, response = MessageApiDTO.class, message = "Sucesso"),
			@ApiResponse(code = 400, message = "Bad Request"),
			@ApiResponse(code = 500, message = "Internal Server Error")
	})
	public MessageApiDTO updateUsuarioById(@PathVariable Long id, @RequestBody @Valid UsuarioDTO dto) {
		return service.updateUsuarioById(id, dto);
	}

}
