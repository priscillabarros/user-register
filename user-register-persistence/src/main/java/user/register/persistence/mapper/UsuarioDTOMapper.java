package user.register.persistence.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.statement.StatementContext;

import user.register.dto.usuario.UsuarioDTO;


public class UsuarioDTOMapper implements RowMapper<UsuarioDTO>{

	@Override
	public UsuarioDTO map(ResultSet rs, StatementContext ctx) throws SQLException {
		return UsuarioDTO.builder()
				.id(rs.getLong("id"))
				.nome(rs.getString("nome"))
				.numeroDocumento(rs.getString("numeroDocumento"))
				.telefoneContato(rs.getString("telefoneContato")).build();
	}
	
	
}
