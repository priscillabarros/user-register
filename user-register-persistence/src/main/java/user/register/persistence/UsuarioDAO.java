package user.register.persistence;

import java.util.Optional;
import java.util.Set;

import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlCall;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.statement.UseRowMapper;
import org.springframework.stereotype.Repository;

import user.register.dto.usuario.UsuarioDTO;
import user.register.persistence.mapper.UsuarioDTOMapper;
import user.register.sql.log.factory.LogSqlFactory;

/**
 * @author priscilla.barros 
 * Classe para DAO (Data Acess Object) do Usuario
 */
@Repository
@LogSqlFactory
public interface UsuarioDAO {

	/**
	 * Salvar Usuario
	 * 
	 * @return {@link Long} - ID da Usuario persistido
	 */
	@GetGeneratedKeys
	@SqlUpdate("INSERT INTO usuario(id, nome, numeroDocumento, telefoneContato) VALUES(:id, :nome, :numeroDocumento, :telefoneContato)")
	public Long save(@BindBean UsuarioDTO usuario);

	/**
	 * Remover Usuario por ID informado
	 * 
	 * @param id {@link Long} - ID da Usuario removido
	 */
	@SqlCall("delete from usuario where id = :id")
	public void removeUsuario(@Bind("id") Long id);

	/**
	 * Recuperar Usuario por ID informado
	 * 
	 * @return {@link Optional<Usuario}
	 */
	@SqlQuery("select * from usuario where id = :id")
	@UseRowMapper(UsuarioDTOMapper.class)
	public Optional<UsuarioDTO> find(@Bind("id") Long id);

	/**
	 * Recuperar todos as Usuarios em base de dados.
	 * 
	 * @return {@link Iterable<Usuario>}
	 */
	@SqlQuery("select * from usuario")
	@UseRowMapper(UsuarioDTOMapper.class)
	Set<UsuarioDTO> findAll();

	/**
	 * Recuperar proximo valor da sequence usuario_seq
	 * 
	 * @return {@link Long}
	 */
	@SqlQuery("select nextval('usuario_seq')")
	Long getSequence();

	/**
	 * Atualizar Usuario por ID informado
	 * 
	 * @param usuario {@link UsuarioDTO} - Usuario a ser atualizado
	 */
	@SqlUpdate("UPDATE usuario set nome = :nome, numeroDocumento = :numeroDocumento, telefoneContato = :telefoneContato where id = :id")
	public void updateUsuario(@BindBean UsuarioDTO usuario);

}
